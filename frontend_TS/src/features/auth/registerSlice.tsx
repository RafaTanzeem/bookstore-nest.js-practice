import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';

interface DataType {
    message:string,
    status: number,
}

export const registerUser = createAsyncThunk(
  'user/login',
  async (body, { rejectWithValue }) => {
    console.log('body', body);
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const res:any = await axios.post(
        'http://localhost:3001/api/user',
        body,
        config,
      );
      const  data :DataType = res.data;

      return data.message;
    } catch (err) {
      console.log(err);
      return rejectWithValue(err);
    }
  },
);

interface UsersState {
  message: string;
  loading: 'idle' | 'pending' | 'succeeded' | 'failed';
  success: boolean;
  error: unknown;
}

const initialState: UsersState = {
  message: '',
  loading: 'idle',
  success: false,
  error: null,
};

// Then, handle actions in your reducers:
const registerSlice = createSlice({
  name: 'register',
  initialState,
  reducers: {
    // standard reducer logic, with auto-generated action types per reducer
  },
  extraReducers: (builder) => {
    builder.addCase(registerUser.pending, (state, action) => {
      state.loading = 'pending';
      state.success = false;
    });
    builder.addCase(registerUser.fulfilled, (state, action:PayloadAction<string>) => {
      state.loading = "succeeded";
      state.success = true;
      console.log('message', action.payload);
      state.message = action.payload;
    });
    builder.addCase(registerUser.rejected, (state, action) => {
      state.loading = 'failed';
      state.success = false;
      state.error = action.payload;
    });
  },
});

export default registerSlice.reducer;

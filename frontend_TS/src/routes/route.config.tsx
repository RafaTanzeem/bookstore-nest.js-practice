const route = {
  home: '/',
  login: '/login',
  register: '/register',
  profile: '/profile',
  addbook: '/book',
};

export default route;

import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useDispatch } from 'react-redux';
import route from '../router.config ';
import { logoutUser } from '../slices.js/auth/loginSlice';
export default function Header() {
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(logoutUser());
  };
  return (
    <div>
      <Navbar bg="primary" variant="dark">
        <Container>
          <Navbar.Brand href={route.home}>Book_Store</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href={route.home}>Home</Nav.Link>
            <Nav.Link href={route.profile}>Profile</Nav.Link>
            <Nav.Link href={route.addbook}>Books</Nav.Link>
          </Nav>
          <Nav className="nav-text">
            <Nav.Link href={route.login}>login</Nav.Link>
            <Nav.Link href={route.login} onClick={logout}>
              logout
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </div>
  );
}

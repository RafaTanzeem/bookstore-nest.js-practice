import React from 'react';

import { Navigate } from 'react-router-dom';

function ProtectedRoute({ children, redirectPath, userInfo }) {
  if (!userInfo) {
    return <Navigate to={redirectPath} replace />;
  } else return children;
}

export default ProtectedRoute;

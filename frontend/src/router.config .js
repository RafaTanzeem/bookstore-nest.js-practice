const route = {
  login: '/login',
  register: '/register',
  home: '/',
  profile: '/profile',
  addbook: '/book',
};

export default route;

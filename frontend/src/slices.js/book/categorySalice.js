import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getCategories = createAsyncThunk(
  'books/getCategories',
  async () => {
    try {
      const token = JSON.parse(localStorage.getItem('userInfo')).access_token;

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      };

      const res = await axios.get('http://localhost:3001/api/category', config);

      const { data } = res;
      console.log(res);
      return data;
    } catch (err) {
      console.log(err);
      return err;
    }
  },
);

const initialState = {
  loading: false,
  success: false,
  data: [],
};
const getCategoriesSlice = createSlice({
  name: 'getCategories',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCategories.pending, (state, action) => {
      state.loading = true;
      state.success = false;
    });
    builder.addCase(getCategories.fulfilled, (state, action) => {
      state.loading = false;
      state.success = true;
      console.log('category', action.payload);
      state.data = action.payload;
    });
    builder.addCase(getCategories.rejected, (state, action) => {
      state.loading = false;
      state.success = false;
      state.error = action.payload;
    });
  },
});

// export const {} = registerSlice.actions;
export default getCategoriesSlice.reducer;

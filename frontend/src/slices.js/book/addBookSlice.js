import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const addBook = createAsyncThunk(
  'user/addBook',
  async (body, { rejectWithValue }) => {
    console.log('body', body);
    try {
      const token = JSON.parse(localStorage.getItem('userInfo')).access_token;

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      };

      const res = await axios.post(
        `http://localhost:3001/api/book`,
        body,
        config,
      );
      const { data } = res;
      console.log(res);
      return data;
    } catch (err) {
      console.log(err);
      return rejectWithValue(err.response.data);
    }
  },
);

const initialState = {
  loading: false,
  success: false,
  message: {},
};
const addBookSlice = createSlice({
  name: 'addBook',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(addBook.pending, (state, action) => {
      state.loading = true;
      state.success = true;
    });
    builder.addCase(addBook.fulfilled, (state, { payload }) => {
      state.loading = false;
      console.log('payload', payload);
      state.success = true;
      state.message = payload;
    });
    builder.addCase(addBook.rejected, (state, action) => {
      state.loading = false;
      state.success = true;
      state.message = action.payload;
    });
  },
});

// export const {} = registerSlice.actions;
export default addBookSlice.reducer;

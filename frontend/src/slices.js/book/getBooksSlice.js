import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getAllBooks = createAsyncThunk('books/getAllBooks', async () => {
  try {
    const token = JSON.parse(localStorage.getItem('userInfo')).access_token;

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios.get('http://localhost:3001/api/book', config);

    const { data } = res;
    console.log(data.books);
    return data.books;
  } catch (err) {
    console.log(err);
    return err;
  }
});

const initialState = {
  loading: false,
  success: false,
  data: [],
};
const getBooksSlice = createSlice({
  name: 'getBooks',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllBooks.pending, (state, action) => {
      state.loading = true;
      state.success = false;
    });
    builder.addCase(getAllBooks.fulfilled, (state, action) => {
      state.loading = false;
      state.success = true;
      console.log('books', action.payload);
      state.data = action.payload;
    });
    builder.addCase(getAllBooks.rejected, (state, action) => {
      state.loading = false;
      state.success = false;
      state.error = action.payload;
    });
  },
});

// export const {} = registerSlice.actions;
export default getBooksSlice.reducer;

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const loginUser = createAsyncThunk(
  'user/loginUser',
  async (body, { rejectWithValue }) => {
    console.log('body', body);
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const res = await axios.post(
        `http://localhost:3001/api/auth/login`,
        body,
        config,
      );
      const { data } = res;
      console.log(data);
      localStorage.setItem('userInfo', JSON.stringify(data));
      return data;
    } catch (err) {
      console.log(err);
      return rejectWithValue(err.response.data);
    }
  },
);

export const logoutUser = createAsyncThunk('user/logoutUser', () => {
  localStorage.removeItem('userInfo');
});

const initialState = {
  loading: false,
  success: false,
  userInfo: {},
};
const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(loginUser.pending, (state, action) => {
      state.loading = true;
      state.success = true;
    });
    builder.addCase(loginUser.fulfilled, (state, { payload }) => {
      state.loading = false;
      console.log('payload', payload);
      state.success = true;
      state.userInfo = payload;
    });
    builder.addCase(loginUser.rejected, (state, action) => {
      state.loading = false;
      state.success = true;
      state.userInfo = action.payload;
    });
  },
});

// export const {} = registerSlice.actions;
export default loginSlice.reducer;

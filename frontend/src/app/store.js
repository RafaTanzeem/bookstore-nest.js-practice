import { configureStore } from '@reduxjs/toolkit';
import registerReducer from '../slices.js/auth/registerSlice';
import loginReducer from '../slices.js/auth/loginSlice';
import getBooksReducer from '../slices.js/book/getBooksSlice';
import getCategoriesReducer from '../slices.js/book/categorySalice';
import addBookReducer from '../slices.js/book/addBookSlice';
export const store = configureStore({
  reducer: {
    register: registerReducer,
    login: loginReducer,
    getBooks: getBooksReducer,
    addBook: addBookReducer,
    getCategories: getCategoriesReducer,
  },
});

import React from 'react';
import { useForm } from 'react-hook-form';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import toast from 'react-hot-toast';
import FormContainer from '../components/FormContainer';
import { useDispatch, useSelector } from 'react-redux';
import { registerUser } from '../slices.js/auth/registerSlice';
import { useNavigate } from 'react-router';
import route from '../router.config ';
import { Link } from 'react-router-dom';

export default function Register() {
  const registerState = useSelector((state) => state.register);
  const { message, error, success } = registerState;
  console.log(message, error, success);

  const navigate = useNavigate();

  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    console.log(data);
    dispatch(registerUser(data));
    if (error && success === false) {
      return toast.error(`'Not registered Something went wrong'${error}`);
    }
    if (success === true) {
      toast.success(message);
      navigate(route.login);
    }
  };
  return (
    <div className="container">
      <FormContainer>
        <h1>
          Register <i className="fa fa-upload" aria-hidden="true"></i>
        </h1>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3">
            <Form.Label>Name</Form.Label>
            <Form.Control
              placeholder="your name"
              {...register('name', { required: true, minLength: 3 })}
            />
            {errors.name && <p>name should be minimum 3 characters</p>}
          </Form.Group>

          <Form.Group>
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              {...register('email', {
                required: true,
                pattern:
                  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
              })}
            />
            <p>{errors.email?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              required={true}
              type="password"
              placeholder="Password"
              {...register('password', { required: true, minLength: 8 })}
            />
            {errors.password && <p>password should be minimum 8 characters</p>}
          </Form.Group>
          <Link to={route.login}>
            <p>Already have an account?</p>
          </Link>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </FormContainer>
    </div>
  );
}

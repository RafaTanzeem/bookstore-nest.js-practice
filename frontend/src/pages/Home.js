import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import React, { useEffect, useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { useDispatch, useSelector } from 'react-redux';
import { getAllBooks } from '../slices.js/book/getBooksSlice';

export default function Home() {
  const [index, setIndex] = useState(0);

  const getBooks = useSelector((state) => state.getBooks);

  const { data } = getBooks;

  const dispatch = useDispatch();

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  useEffect(() => {
    console.log('useEffect');
    dispatch(getAllBooks());
  }, []);
  return (
    <div className="container">
      {/* Carousel */}
      <div className=" slider">
        <Carousel activeIndex={index} onSelect={handleSelect}>
          <Carousel.Item>
            <img
              className="d-block w-100 img"
              src="http://localhost:3001/1674826623903-489794399.jpeg"
              alt="First slide"
            />
            <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100 img"
              src="http://localhost:3001/1674819520031-788593383.jpeg"
              alt="Second slide"
            />

            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100 img"
              src="http://localhost:3001/1674826623903-489794399.jpeg"
              alt="Third slide"
            />

            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>
                Praesent commodo cursus magna, vel scelerisque nisl consectetur.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>

      {/* Cards */}
      <div className="d-flex justify-content-around wrapper">
        {data.map((book) => {
          return (
            <Card style={{ width: '18rem' }} key={book.id}>
              <Card.Img
                variant="top"
                src={`http://localhost:3001/${book.image}`}
              />
              <Card.Body>
                <Card.Title>{book.title}</Card.Title>
                <Card.Text>{book.desc}</Card.Text>
                <div>
                  <Button variant="primary" size="sm">
                    Buy {book.price}$
                  </Button>{' '}
                  <Button variant="secondary" size="sm">
                    Lease {book.lendingPrice}$
                  </Button>
                </div>
              </Card.Body>
            </Card>
          );
        })}
      </div>
    </div>
  );
}

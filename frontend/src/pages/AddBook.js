import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormContainer from '../components/FormContainer';
import { useSelector, useDispatch } from 'react-redux';
import { getCategories } from '../slices.js/book/categorySalice';
import { addBook } from '../slices.js/book/addBookSlice';

import toast from 'react-hot-toast';
import { useNavigate } from 'react-router';
import route from '../router.config ';

export default function AddBook() {
  const categorySlice = useSelector((state) => state.getCategories);
  const { data } = categorySlice;
  console.log(data);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getCategories());
  }, []);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log('data:-', data);
    dispatch(addBook(data));
    toast.success('book reated');
    // navigate(route.home);
  };
  return (
    <div className="container">
      <FormContainer>
        <h1>
          Add Book <i className="fa fa-upload" aria-hidden="true"></i>
        </h1>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Book Title</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Book Title"
              {...register('title', {
                required: true,
                minLength: 1,
              })}
            />
            <p>{errors.title?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Book Author</Form.Label>
            <Form.Control
              type="text"
              placeholder="Author Name"
              {...register('author', { required: true, minLength: 3 })}
            />
            <p>{errors.author?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Book Desc</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Book summary"
              {...register('desc', {
                required: true,
                minLength: 5,
              })}
            />
            <p>{errors.desc?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Book Image</Form.Label>
            <Form.Control
              type="file"
              placeholder="chose book"
              {...register('image', {
                required: true,
              })}
            />
            <p>{errors.image?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Book Price $</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Book price"
              {...register('price', {
                required: true,
                default: 0.0,
              })}
            />
            <p>{errors.price?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Book Lease Price $</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Book Lease"
              {...register('lendingPrice', {
                required: true,
                default: 0.0,
              })}
            />
            <p>{errors.lendingPrice?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Book Quantity </Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Book Quantity"
              {...register('qty', {
                required: true,
                default: 0,
              })}
            />
            <p>{errors.qty?.message}</p>
          </Form.Group>

          {data.map((category) => {
            return (
              <div key={category.id}>
                <input
                  {...register('categoriesId')}
                  type="checkbox"
                  value={category.id}
                />
                <span>{category.name}</span>
              </div>
            );
          })}

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </FormContainer>
    </div>
  );
}

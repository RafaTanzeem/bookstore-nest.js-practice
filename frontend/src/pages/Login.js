import React from 'react';
import { useForm } from 'react-hook-form';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormContainer from '../components/FormContainer';
import { Link, useNavigate } from 'react-router-dom';
import route from '../router.config ';
import { loginUser } from '../slices.js/auth/loginSlice';
import { useSelector, useDispatch } from 'react-redux';
import toast from 'react-hot-toast';

export default function Login() {
  const loginSlice = useSelector((state) => state.login);

  const { userInfo, error, success } = loginSlice;
  console.log('', userInfo, error, success);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data, e) => {
    console.log(data);
    dispatch(loginUser(data));
    if (error && success === false) {
      return toast.error(`'Not able to signIn Something went wrong'${error}`);
    }
    navigate(route.home);
  };
  return (
    <div className="container">
      <FormContainer>
        <h1>
          LogIn <i className="fa fa-upload" aria-hidden="true"></i>
        </h1>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              {...register('email', {
                required: true,
                pattern:
                  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
              })}
            />
            <p>{errors.email?.message}</p>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              {...register('password', { required: true, minLength: 8 })}
            />
            {errors.password && <p>password should be minimum 8 characters</p>}
          </Form.Group>
          <Link to={route.register}>
            <p>New user ?</p>
          </Link>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </FormContainer>
    </div>
  );
}

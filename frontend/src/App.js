import { Route, Routes } from 'react-router';
import route from './router.config ';
import Login from './pages/Login';
import Register from './pages/Register';
import ProtectedRoute from './components/ProtectedRoute';
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './pages/Home';
import { Toaster } from 'react-hot-toast';
import './app.css';
import Profile from './pages/Profile';
import AddBook from './pages/AddBook';
function App() {
  const userInfo = JSON.parse(localStorage.getItem('userInfo'));
  const userName = userInfo ? userInfo.user.name : 'guest';
  return (
    <div className="App ">
      <Toaster />
      <Header />
      <main className="py-3">
        <Routes>
          <Route
            path={route.home}
            element={
              <ProtectedRoute redirectPath={route.login} userInfo={userInfo}>
                <Home />
              </ProtectedRoute>
            }
          />
          <Route
            path={route.profile}
            element={
              <ProtectedRoute redirectPath={route.login} userInfo={userInfo}>
                <Profile />
              </ProtectedRoute>
            }
          />
          <Route
            path={route.addbook}
            element={
              <ProtectedRoute redirectPath={route.login} userInfo={userInfo}>
                <AddBook />
              </ProtectedRoute>
            }
          />

          <Route
            path={route.login}
            element={
              <ProtectedRoute
                redirectPath={route.home}
                userInfo={userInfo ? false : true}
              >
                <Login />
              </ProtectedRoute>
            }
          ></Route>
          <Route path={route.register} element={<Register />}></Route>
        </Routes>
      </main>
      <Footer userName={userName} />
    </div>
  );
}

export default App;

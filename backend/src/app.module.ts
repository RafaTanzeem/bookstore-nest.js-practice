import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { BooksModule } from './books/books.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InvoiceModule } from './invoice/invoice.module';

import entities from './entities';
@Module({
  imports: [
    UserModule,
    BooksModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'root',
      password: 'rafa2000tanzeem',
      database: 'book_store',
      entities: entities,
      synchronize: true,
    }),
    InvoiceModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

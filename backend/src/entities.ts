import { Book } from './books/typeorm/Book.entity';
import { Category } from './books/typeorm/Category.entity';
import { Invoice } from './invoice/typeorm/Invoice.entity';
import { Profile } from './user/typeorm/Profile.entity';
import { User } from './user/typeorm/User.entity';

export default [User, Book, Category, Invoice, Profile];

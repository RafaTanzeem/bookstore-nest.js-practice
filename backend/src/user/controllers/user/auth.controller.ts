import { GoogleAuthGuard } from '../../authGuards/Google.guard';
// import { JwtAuthGuard } from '../../authGuards/Jwt.guard';
import { AuthService } from '../../services/user/auth.service';
import {
  Controller,
  Post,
  UseGuards,
  Request,
  Inject,
  Get,
  Session,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
  constructor(
    @Inject('AUTH_SERVICE') private readonly authService: AuthService,
  ) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req) {
    console.log('inside login');
    return this.authService.login(req.user);
  }

  @Get()
  authSession(@Session() session: Record<string, any>) {
    console.log(session);
  }

  @Get('google/login')
  @UseGuards(AuthGuard('google'))
  googleLogin() {
    console.log('inside google login');
    return { message: 'google login' };
  }

  @Get('google/redirect')
  @UseGuards(GoogleAuthGuard)
  googleRedirect() {
    return { message: 'Ok' };
  }
}

import {
  Body,
  Controller,
  Request,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateProfileDto } from 'src/user/dtos/CretaeProfile.dto';
import { UpdateProfileDto } from 'src/user/dtos/UpdateProfile.dto';
import { ProfileService } from 'src/user/services/user/profile.service';

@Controller('profile')
export class ProfileController {
  constructor(
    @Inject('PROFILE_SERVICE') private readonly profileService: ProfileService,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getUserProfile(@Request() req) {
    const profile = await this.profileService.findProfileByUserId(req.user.id);
    if (!profile)
      throw new HttpException('Profile Not Found', HttpStatus.NOT_FOUND);
    return { status: HttpStatus.OK, profile };
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createProfile(
    @Body() createProfileDto: CreateProfileDto,
    @Request() req,
  ) {
    console.log(createProfileDto, req.user);
    const profile = await this.profileService.createProfile(
      createProfileDto,
      req.user.id,
    );
    if (!profile)
      throw new HttpException('server Error', HttpStatus.SERVICE_UNAVAILABLE);

    return { status: HttpStatus.CREATED, profile };
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async editProfile(
    @Param('id') id: string,
    @Body() updateProfileDto: UpdateProfileDto,
  ) {
    const updatedProfile = this.profileService.updateProfileById(
      id,
      updateProfileDto,
    );
    if (!updatedProfile)
      throw new HttpException('Server Error', HttpStatus.SERVICE_UNAVAILABLE);

    return { status: HttpStatus.OK, updatedProfile };
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getProfileById(@Param('id') id: string) {
    const profile = await this.profileService.findProfileById(id);
    if (!profile)
      throw new HttpException('Profile Not Found', HttpStatus.NOT_FOUND);
    return { status: HttpStatus.OK, profile };
  }
}

import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Post,
  Request,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateUserDto } from 'src/user/dtos/CreateUser.dto';
import { UserService } from 'src/user/services/user/user.service';
// import { User } from 'src/user/typeorm/User.entity';
@Controller('user')
export class UserController {
  constructor(
    @Inject('USER_SERVICE') private readonly userService: UserService,
  ) {}

  @Get()
  async getAllUsers() {
    const users = await this.userService.findAllUsers();
    return { status: HttpStatus.OK, users };
  }

  // @Get(':id')
  // async getUserById(@Param('id') id: string) {
  //   console.log('inside get user by id');
  //   const user = await this.userService.findUserById(id);
  //   if (!user) throw new HttpException('User not found', HttpStatus.NOT_FOUND);
  //   return { status: HttpStatus.OK, user };
  // }

  @Post()
  @UsePipes(ValidationPipe)
  async createUser(@Body() createUserDto: CreateUserDto) {
    console.log(createUserDto);
    const userExisted = await this.userService.findUserByEmail(
      createUserDto.email,
    );
    if (userExisted) {
      throw new HttpException('user already existed', HttpStatus.FORBIDDEN);
    }
    await this.userService.createUser(createUserDto);
    return { status: HttpStatus.CREATED, message: 'Registered Successfuly' };
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('profile')
  async getUserProfile(@Request() req) {
    console.log(req.user.id);
    const profile = await this.userService.findUserProfile(req.user.id);

    return profile;
  }
}

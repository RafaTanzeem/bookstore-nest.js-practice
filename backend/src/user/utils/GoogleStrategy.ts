import { Inject, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, Profile } from 'passport-google-oauth20';
import { CreateUserDto } from '../dtos/CreateUser.dto';
import { UserService } from '../services/user/user.service';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject('USER_SERVICE') private readonly userService: UserService,
  ) {
    super({
      clientID:
        '920193834761-2t1gbut7r6e6slpu812vkfi2lfh41650.apps.googleusercontent.com',
      clientSecret: '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',
      callbackURL: 'http://localhost:3001/api/auth/google/redirect',
      scope: ['profile', 'email'],
    });
  }
  async validate(accessToken: string, refreshToken: string, profile: Profile) {
    console.log('inside google strategy');
    console.log(
      'token:',
      accessToken,
      'r-token:',
      refreshToken,
      'profile',
      profile,
    );
    const createUser: CreateUserDto = {
      name: profile.displayName,
      email: profile.emails[0].value,
      password: null,
    };
    const user = await this.userService.createUser(createUser);
    console.log('google strategy', user);
    return user;
  }
}

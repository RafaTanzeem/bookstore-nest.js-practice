import { encodePassword } from '../../utils/bcrypt';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from 'src/user/dtos/CreateUser.dto';
import { User } from 'src/user/typeorm/User.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async findAllUsers() {
    return await this.userRepository.find();
  }

  async createUser(createUserDto: CreateUserDto) {
    try {
      console.log('createuser');

      if (createUserDto.password) {
        createUserDto.password = await encodePassword(createUserDto.password);
      }
      console.log('createuser');
      const newUser = this.userRepository.create(createUserDto);

      return await this.userRepository.save(newUser);
    } catch (err) {
      console.log('err', err.code);
    }
  }

  async findUserByEmail(email: string) {
    return await this.userRepository.findOneBy({ email });
  }

  async findUserById(id: string) {
    return await this.userRepository.findOne({
      where: { id },
      relations: ['profile'],
    });
  }
  async findUserProfile(id: string) {
    return await this.userRepository.findOne({
      where: { id },
      relations: ['profile'],
    });
  }
}

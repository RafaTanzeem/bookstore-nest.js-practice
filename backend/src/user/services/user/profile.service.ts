import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProfileDto } from 'src/user/dtos/CretaeProfile.dto';
import { UpdateProfileDto } from 'src/user/dtos/UpdateProfile.dto';
import { Profile } from 'src/user/typeorm/Profile.entity';
import { Repository } from 'typeorm';
import { UserService } from './user.service';

@Injectable()
export class ProfileService {
  constructor(
    @Inject('USER_SERVICE') private readonly userService: UserService,

    @InjectRepository(Profile)
    private readonly profileRepository: Repository<Profile>,
  ) {}

  async createProfile(createProfileDto: CreateProfileDto, userId: string) {
    const { username, address, gender, pic, phone } = createProfileDto;
    console.log(userId);
    const user = await this.userService.findUserById(userId);
    if (user) {
      const profile = this.profileRepository.create({
        username,
        address,
        gender,
        pic,
        phone,
        user,
      });

      return await this.profileRepository.save(profile);
    }
    console.log('no user with this id');
  }

  async findProfileByUserId(userId: string) {
    const user = await this.userService.findUserById(userId);
    console.log(user.profile.id);
    const id = user.profile.id;
    const profile = await this.profileRepository.findOne({
      where: { id },
      relations: ['user', 'invoices'],
    });

    return profile;
  }

  async updateProfileById(id: string, updateProfileDto: UpdateProfileDto) {
    const profile = await this.profileRepository.findOneBy({ id });
    for (const key in updateProfileDto) {
      if (key) profile.key = updateProfileDto[key];
    }
    return await this.profileRepository.save(profile);
  }

  async findProfileById(id: string) {
    return await this.profileRepository.findOne({
      where: { id },
      // relations: ['user', 'invoices'],
    });
  }
}

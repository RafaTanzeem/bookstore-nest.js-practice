import {
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { UserService } from './user.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @Inject('USER_SERVICE') private readonly userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string) {
    try {
      const userDB = await this.userService.findUserByEmail(email);

      console.log('inside userValidation', userDB);

      if (userDB && bcrypt.compareSync(password, userDB.password))
        return userDB;

      return null;
    } catch (err) {
      console.log(err);
      throw new InternalServerErrorException();
    }
  }

  async login(user: any) {
    const payload = { sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
      user,
    };
  }
}

import { Invoice } from 'src/invoice/typeorm/Invoice.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Gender } from '../dtos/CretaeProfile.dto';
import { User } from './User.entity';

@Entity()
export class Profile {
  [x: string]: any;
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    name: 'username',
    nullable: false,
  })
  username: string;

  @Column({
    name: 'address',
    nullable: false,
  })
  address: string;

  @Column({
    name: 'gender',
    default: Gender.MALE,
  })
  gender: Gender;

  @Column({
    name: 'profile_pic',
  })
  pic: string;

  @Column({
    name: 'phone',
  })
  phone: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToOne(() => User, (user) => user.profile)
  @JoinColumn()
  user: User;

  @OneToMany(() => Invoice, (invoice) => invoice.profile)
  invoices: Invoice[];
}

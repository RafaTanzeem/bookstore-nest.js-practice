import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Invoice } from 'src/invoice/typeorm/Invoice.entity';
import { GoogleAuthGuard } from './authGuards/Google.guard';
// import { JwtAuthGuard } from './authGuards/Jwt.guard';
import { AuthController } from './controllers/user/auth.controller';
import { ProfileController } from './controllers/user/profile.controller';
import { UserController } from './controllers/user/user.controller';
import { AuthService } from './services/user/auth.service';
import { ProfileService } from './services/user/profile.service';
import { UserService } from './services/user/user.service';
import { Profile } from './typeorm/Profile.entity';
import { User } from './typeorm/User.entity';
import { GoogleStrategy } from './utils/GoogleStrategy';
import { jwtConstants } from './utils/jwtConstants';
import { JwtStrategy } from './utils/JwtStrategy';
import { LocalStrategy } from './utils/LocalStrategy';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Profile, Invoice]),
    PassportModule.register({
      session: true,
    }),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60s' },
    }),
  ],

  controllers: [UserController, ProfileController, AuthController],
  providers: [
    {
      provide: 'USER_SERVICE',
      useClass: UserService,
    },
    {
      provide: 'AUTH_SERVICE',
      useClass: AuthService,
    },
    // {
    //   provide: 'APP_GUARD',
    //   useClass: JwtAuthGuard,
    // },
    {
      provide: 'GOOGLE_GUARD',
      useClass: GoogleAuthGuard,
    },
    {
      provide: 'PROFILE_SERVICE',
      useClass: ProfileService,
    },

    LocalStrategy,
    JwtStrategy,
    GoogleStrategy,
  ],
})
export class UserModule {}

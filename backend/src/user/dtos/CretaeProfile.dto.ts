import { IsNotEmpty, IsPhoneNumber, Length } from 'class-validator';

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
  OTHER = 'other',
}
export class CreateProfileDto {
  @IsNotEmpty()
  @Length(3, 20)
  username: string;

  @IsNotEmpty()
  @Length(5, 100)
  address: string;

  @IsNotEmpty()
  gender: Gender;

  pic: string;

  // @IsPhoneNumber()
  phone: number;
}

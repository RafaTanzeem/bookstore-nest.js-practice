import { IsPhoneNumber, Length } from 'class-validator';
import { Gender } from './CretaeProfile.dto';

export class UpdateProfileDto {
  @Length(3, 20)
  username: string;

  @Length(5, 100)
  address: string;

  gender: Gender;

  pic: string;

  @IsPhoneNumber()
  phone: number;
}

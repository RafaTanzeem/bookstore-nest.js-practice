import { Invoice } from 'src/invoice/typeorm/Invoice.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Category } from './Category.entity';

@Entity()
export class Book {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    name: 'book_image',
    nullable: false,
  })
  image: string;

  @Column({
    name: 'user_name',
    nullable: false,
    unique: true,
  })
  title: string;

  @Column({
    name: 'author',
    nullable: false,
  })
  author: string;

  @Column({
    name: 'description',
  })
  desc: string;

  @Column({
    name: 'sale_price',
    default: 0.0,
  })
  price: number;

  @Column({
    name: 'lending_price',
    default: 0.0,
  })
  lendingPrice: number;

  @Column({
    name: 'quantity',
    default: 0.0,
  })
  qty: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToMany(() => Invoice, (invoice) => invoice.books)
  invoice: Invoice[];

  @ManyToMany(() => Category, (category) => category.books)
  @JoinTable()
  categories: Category[];
}

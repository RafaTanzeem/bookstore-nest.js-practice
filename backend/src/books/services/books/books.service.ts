import { Inject, Injectable } from '@nestjs/common';
import { CreateBookDto } from '../../dto/create.book.dto';
import { Book } from 'src/books/typeorm/Book.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
// import { Category } from 'src/books/typeorm/Category.entity';
import { CategoriesService } from '../categories/categories.service';
import { Category } from 'src/books/typeorm/Category.entity';
@Injectable()
export class BooksService {
  constructor(
    @Inject('CATEGORY_SERVICE')
    private readonly categoriesService: CategoriesService,
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
  ) {}

  async findAllBooks() {
    return await this.bookRepository.find();
  }

  async createBook(createBookDto: CreateBookDto) {
    console.log('inside create book service');
    const {
      categoriesId,
      title,
      desc,
      author,
      image,
      price,
      lendingPrice,
      qty,
    } = createBookDto;
    const categories: Array<Category> =
      await this.categoriesService.findCategoriesWithArrayOfIds(categoriesId);
    console.log(categories);
    const book = this.bookRepository.create({
      title,
      desc,
      author,
      price,
      image,
      lendingPrice,
      qty,
      categories,
    });
    return await this.bookRepository.save(book);
  }

  async findBookById(id: string) {
    const book = await this.bookRepository.findOne({
      where: { id },
      relations: ['categories'],
    });
    return book;
  }

  async findBookByTitle(title: string) {
    return await this.bookRepository.findOneBy({ title });
  }

  async deleteBookById(id: string) {
    return await this.bookRepository.delete({ id });
  }
}

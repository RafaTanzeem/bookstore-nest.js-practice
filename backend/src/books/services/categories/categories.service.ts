import { In, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Category } from 'src/books/typeorm/Category.entity';
import { CreateCategoryDto } from 'src/books/dto/create.categories.dto';
@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
  ) {}

  async createCategory(
    createCategoryDto: CreateCategoryDto,
  ): Promise<Category | null> {
    const newCategory = this.categoryRepository.create(createCategoryDto);
    return this.categoryRepository.save(newCategory);
  }
  async findCategoriesWithArrayOfIds(categoriesId: Array<string>) {
    return await this.categoryRepository.find({
      where: { id: In(categoriesId) },
    });
  }
  async findAllCategories() {
    return await this.categoryRepository.find();
  }

  async findCategoryByName(name: string) {
    const category = await this.categoryRepository.findOne({
      where: { name },
      relations: ['books'],
    });
    return category;
  }
  async findCategoryById(id: string) {
    const category = await this.categoryRepository.findOneBy({ id });
    return category;
  }

  async deleteCategoryById(id: string) {
    return await this.categoryRepository.delete({ id });
  }
}

import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  Post,
} from '@nestjs/common';
import { CreateCategoryDto } from 'src/books/dto/create.categories.dto';
import { CategoriesService } from 'src/books/services/categories/categories.service';

@Controller('category')
export class CategoriesController {
  constructor(
    @Inject('CATEGORY_SERVICE') private categoriesService: CategoriesService,
  ) {}

  @Get()
  async getAllCategories() {
    return await this.categoriesService.findAllCategories();
  }

  @Post()
  async postCategory(@Body() createCategoryDto: CreateCategoryDto) {
    const categoryExisted = await this.categoriesService.findCategoryByName(
      createCategoryDto.name,
    );
    if (categoryExisted)
      throw new HttpException('Category Already Existed', HttpStatus.FOUND);

    const category = await this.categoriesService.createCategory(
      createCategoryDto,
    );
    throw new HttpException(
      `Category Created : ${category.name}`,
      HttpStatus.CREATED,
    );
  }

  @Get(':name')
  async getCategoryByName(@Param('name') name: string) {
    const category = await this.categoriesService.findCategoryByName(name);
    return category;
  }
}

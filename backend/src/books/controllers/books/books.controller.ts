import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  ParseFilePipe,
  ParseIntPipe,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { CreateBookDto } from 'src/books/dto/create.book.dto';
import { BooksService } from '../../services/books/books.service';

@Controller('book')
export class BooksController {
  constructor(@Inject('BOOK_SERVICE') private booksService: BooksService) {}

  @Get()
  async getAllBooks() {
    const books = await this.booksService.findAllBooks();
    return { status: HttpStatus.OK, books };
  }

  @Post('file')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, callback) => {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          const ext = extname(file.originalname);
          const filename = `${uniqueSuffix}${ext}`;
          console.log('filename', filename);
          req.body.filename = filename;
          console.log(req.body);
          callback(null, filename);
        },
      }),
    }),
  )
  uploadFile(@Body() name: string, @UploadedFile() file: Express.Multer.File) {
    // console.log('file:', file);
    console.log('name2:', name);
    console.log('inside');
    return 'done upload';
  }
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, callback) => {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          const ext = extname(file.originalname);
          const filename = `${uniqueSuffix}${ext}`;
          req.body.image = filename;
          // console.log('filename', filename);
          callback(null, filename);
        },
      }),
    }),
  )
  async createBook(@Body() createBookDto: CreateBookDto) {
    console.log(createBookDto);
    const bookExisted = await this.booksService.findBookByTitle(
      createBookDto.title,
    );
    if (bookExisted)
      throw new HttpException(
        'Book already existed with this title',
        HttpStatus.CONFLICT,
      );

    const book = await this.booksService.createBook(createBookDto);
    return { status: HttpStatus.CREATED, message: 'Book created', book };
  }

  @Get(':id')
  async getBookById(@Param('id') id: string) {
    const book = this.booksService.findBookById(id);
    if (!book)
      throw new HttpException('No book with this Id', HttpStatus.NOT_FOUND);

    return { status: HttpStatus.FOUND, book };
  }
  @Delete(':id')
  async deleteBook(@Param('id') id: string) {
    const book = await this.booksService.deleteBookById(id);
    return { status: HttpStatus.OK, book };
  }
}

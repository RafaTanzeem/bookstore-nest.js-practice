import { ParseArrayPipe } from '@nestjs/common';
import { Transform } from 'class-transformer';
import { IsInt, IsNotEmpty, Length } from 'class-validator';
import { Category } from '../typeorm/Category.entity';

export class CreateBookDto {
  @IsNotEmpty()
  @Length(1 - 50)
  title: string;

  @IsNotEmpty()
  @Length(3 - 25)
  author: string;

  @Length(5 - 100)
  desc: string;

  @IsNotEmpty()
  image: string;

  @IsInt()
  @Transform(({ value }) => parseInt(value))
  price: number;

  @IsInt()
  @Transform(({ value }) => parseInt(value))
  lendingPrice: number;

  @IsInt()
  @Transform(({ value }) => parseInt(value))
  qty: number;

  @IsNotEmpty()
  // @Transform(({ value }) => JSON.parse(value))
  categoriesId: Array<string>;
}

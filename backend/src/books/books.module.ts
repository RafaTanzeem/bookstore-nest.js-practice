import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Invoice } from 'src/invoice/typeorm/Invoice.entity';
import { BooksController } from './controllers/books/books.controller';
import { BooksService } from './services/books/books.service';
import { CategoriesService } from './services/categories/categories.service';
import { Book } from './typeorm/Book.entity';
import { Category } from './typeorm/Category.entity';
import { CategoriesController } from './controllers/books/categories.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Book, Category, Invoice])],
  controllers: [BooksController, CategoriesController],
  providers: [
    {
      provide: 'BOOK_SERVICE',
      useClass: BooksService,
    },
    {
      provide: 'CATEGORY_SERVICE',
      useClass: CategoriesService,
    },
  ],
})
export class BooksModule {}

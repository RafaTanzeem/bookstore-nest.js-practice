import { Book } from 'src/books/typeorm/Book.entity';
import { Profile } from 'src/user/typeorm/Profile.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { InvoiceType } from '../dtos/CreateInvoice.dto';

@Entity()
export class Invoice {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({
    name: 'type',
    nullable: false,
  })
  invoice: InvoiceType;

  @Column({
    name: 'total_quantity',
    default: 0.0,
  })
  totalQty: number;

  @Column({
    name: 'total_price',
    default: 0.0,
  })
  totalPrice: number;

  @Column({
    name: 'due_date',
  })
  dueDate: Date;

  @ManyToMany(() => Book, (book) => book.invoice)
  @JoinTable()
  books: Book[];

  @ManyToOne(() => Profile, (profile) => profile.invoices)
  @JoinTable()
  profile: Profile;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

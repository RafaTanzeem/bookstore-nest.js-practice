import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from 'src/books/typeorm/Book.entity';
import { Profile } from 'src/user/typeorm/Profile.entity';
import { InvoiceController } from './controllers/invoice/invoice.controller';
import { InvoiceService } from './services/invoice/invoice.service';
import { Invoice } from './typeorm/Invoice.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Book, Invoice, Profile])],
  controllers: [InvoiceController],
  providers: [
    {
      provide: 'INVOICE_SERVICE',
      useClass: InvoiceService,
    },
  ],
})
export class InvoiceModule {}

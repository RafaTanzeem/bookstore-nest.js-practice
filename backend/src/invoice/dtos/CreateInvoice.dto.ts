import { IsNotEmpty } from 'class-validator';
import { Book } from 'src/books/typeorm/Book.entity';
import { Profile } from 'src/user/typeorm/Profile.entity';

export enum InvoiceType {
  SELL = 'sell',
  LEND = 'lended',
}

export class CreateInvoiceDto {
  @IsNotEmpty()
  invoice: InvoiceType;

  @IsNotEmpty()
  totalQty: number;

  @IsNotEmpty()
  totalPrice: number;

  @IsNotEmpty()
  dueDate: Date;

  @IsNotEmpty()
  books: Book[];

  @IsNotEmpty()
  profile: Profile;
}

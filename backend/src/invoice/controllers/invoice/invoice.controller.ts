import {
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  Post,
} from '@nestjs/common';
import { CreateInvoiceDto } from 'src/invoice/dtos/CreateInvoice.dto';
import { InvoiceService } from 'src/invoice/services/invoice/invoice.service';

@Controller('invoice')
export class InvoiceController {
  constructor(
    @Inject('INVOICE_SERVICE') private readonly invoiceService: InvoiceService,
  ) {}

  @Get()
  async getAllInvoices() {
    const invoice = await this.invoiceService.findAllInvoices();
    if (!invoice) throw new HttpException('no invoices', HttpStatus.NOT_FOUND);
    return { status: HttpStatus.OK, invoice };
  }

  @Post()
  async placeInvoice(createInvoiceDto: CreateInvoiceDto) {
    const invoice = await this.invoiceService.createInvoice(createInvoiceDto);
    if (!invoice)
      throw new HttpException('Invoice not created', HttpStatus.FORBIDDEN);
    return { status: HttpStatus.OK, invoice };
  }
  @Delete(':id')
  async cancelInvoice(@Param('id') id: string) {
    const invoice = await this.invoiceService.deleteInvoice(id);
    if (!invoice)
      throw new HttpException('Invoice not found', HttpStatus.FORBIDDEN);
    return { status: HttpStatus.OK, invoice };
  }
}

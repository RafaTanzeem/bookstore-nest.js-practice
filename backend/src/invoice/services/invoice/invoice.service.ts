import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateInvoiceDto } from 'src/invoice/dtos/CreateInvoice.dto';
import { Invoice } from 'src/invoice/typeorm/Invoice.entity';
import { Repository } from 'typeorm';

@Injectable()
export class InvoiceService {
  constructor(
    @InjectRepository(Invoice)
    private readonly invoiceRepository: Repository<Invoice>,
  ) {}

  async findAllInvoices() {
    return await this.invoiceRepository.find();
  }

  async createInvoice(createInvoiceDto: CreateInvoiceDto) {
    const invoice = this.invoiceRepository.create(createInvoiceDto);
    return await this.invoiceRepository.save(invoice);
  }
  async deleteInvoice(id: string) {
    const invoice = await this.invoiceRepository.delete(id);
    return invoice;
  }
}
